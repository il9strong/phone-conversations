#ifndef FILE_READER_H
#define FILE_READER_H

#include "phone_conversations.h"

void read(const char* file_name, phone_conversation* array[], int& size);

#endif