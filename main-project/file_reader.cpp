#include "file_reader.h"
#include "constants.h"

#include <fstream>
#include <cstring>

date convert(char* str)
{
    date result;
    char* context = NULL;
    char* str_number = strtok_s(str, ".", &context);
    result.day = atoi(str_number);
    str_number = strtok_s(NULL, ".", &context);
    result.month = atoi(str_number);
    str_number = strtok_s(NULL, ".", &context);
    result.year = atoi(str_number);
    return result;
}

times convert_time(char* str)
{
    times result;
    char* context = NULL;
    char* str_number = strtok_s(str, ":", &context);
    result.hh = atoi(str_number);
    str_number = strtok_s(NULL, ":", &context);
    result.mm = atoi(str_number);
    str_number = strtok_s(NULL, ":", &context);
    result.ss = atoi(str_number);
    return result;
}

void read(const char* file_name, phone_conversation* array[], int& size)
{
    std::ifstream file(file_name);
    if (file.is_open())
    {
        size = 0;
        char tmp_buffer[MAX_STRING_SIZE];
        while (!file.eof())
        {
            phone_conversation* item = new phone_conversation;
            file >> item->num;
            file >> tmp_buffer;
            item->conversation = convert(tmp_buffer);
            file >> tmp_buffer;
            item->conv_time = convert_time(tmp_buffer);
            file >> item->tarif;
            file >> item->cost;
            array[size++] = item;
        }
        file.close();
    }
    else
    {
        throw "������ �������� �����";
    }
}