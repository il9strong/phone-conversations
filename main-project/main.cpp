#include <iostream>
#include <iomanip>

using namespace std;

#include "phone_conversations.h"
#include "file_reader.h"
#include "constants.h"

int main()
{
    setlocale(LC_ALL, "Russian");
	cout << "������������ ������ #8. GIT\n";
	cout << "������� #9. ���������� ���������\n";
	cout << "�����: ���� �������\n";
    cout << "������: 12\n\n";
    phone_conversation* conversations[MAX_FILE_ROWS_COUNT];
    int size;
    try
    {
        read("data.txt", conversations, size);
        cout << "***** ���������� ��������� *****\n\n";
        for (int i = 0; i < size; i++)
        {
            /********** ����� ��������� **********/
            cout << "������........: ";
            //����� ������
            cout << conversations[i]->num << '\n';
            /********** ����� ���� ��������� **********/
            //����� ����
            cout << "���� ���������...: ";
            cout << setw(4) << setfill('0') << conversations[i]->conversation.year << '-';
            cout << setw(2) << setfill('0') << conversations[i]->conversation.month << '-';
            cout << setw(2) << setfill('0') << conversations[i]->conversation.day;
            cout << '\n';
            //����� ����������������� ���������
            cout << "�����������������...: ";
            cout << conversations[i]->conv_time.hh << ':';
            cout << conversations[i]->conv_time.mm << ':';
            cout << conversations[i]->conv_time.ss << '\n';
            /********** ����� ������ ***********/
            cout << "�����...: ";
            cout << conversations[i]->tarif << '\n';
            cout << "���������...: ";
            cout << conversations[i]->cost << '\n';
            cout << '\n';
            cout << '\n';
        }
        for (int i = 0; i < size; i++)
        {
            delete conversations[i];
        }
    }
    catch (const char* error)
    {
        cout << error << '\n';
    }
	cout << "Laboratory work #8. GIT\n";
	cout << "Variant #9. Phone Conversations\n";
	cout << "Author: Ilya Shitikow\n";
	cout << "Group: 12\n";
	return 0;
}
