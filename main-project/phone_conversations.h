#ifndef PHONE_CONVERSATIONS_H
#define PHONE_CONVERSATIONS_H

#include "constants.h"

struct date
{
    int day;
    int month;
    int year;
};

struct times
{
    int hh;
    int mm;
    int ss;
};

/*struct number
{
    char num[MAX_STRING_SIZE];
    char tarif[MAX_STRING_SIZE];
    double cost;
};*/

struct phone_conversation
{
    char phone[MAX_STRING_SIZE];
    date conversation;
    times conv_time;
    char num[MAX_STRING_SIZE];
    char tarif[MAX_STRING_SIZE];
    double cost;
};

#endif